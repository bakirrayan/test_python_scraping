from src import *
import pytest

def test_request():
    a = http_request('https://httpbin.org/anything', {'isadmin': '1'})
    assert a != None
    b = http_request('https://httpbin.org/anything', {'isadmin': '1'}, 
                 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0')
    assert b != None