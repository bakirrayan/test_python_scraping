import requests, gzip, json, csv, logging

########## Exercice 1 ##########
# send post requet with data and not an obligatiry user agent anr return response body
def http_request(url, params, user_agent=None):
    r = requests.post(url, params=params, headers={'User-Agent': user_agent})
    return r.text if r.status_code == 200 else None # return response body if status code is 200 else return None

########## Exercice 2 ##########
class Products:
    def __init__(self):
        self.products = {}
        self.available_products = []
        self.unavailable_products = []

    # extracting json file from gz archive and return json object
    def import_json(self):
        try:
            with gzip.open("./data/data.json.gz", 'rt', encoding="utf-8") as f:
                self.products = json.load(f)
        except Exception:
            logging.error("Error while importing json file")

    def PrintsAndSave(self):
        products = self.products
        for i in products["Bundles"]:
            try:
                if i["Products"][0]["IsAvailable"] == True:
                    Product_Name = i["Products"][0]["Name"]
                    Product_Price = i["Products"][0]["Price"]
                    Product_IsAvailable = i["Products"][0]["IsAvailable"]
                    Product_IsInStock = i["Products"][0]["IsInStock"]
                    print(f"You can buy {Product_Name[:30]} at our store at {round(Product_Price, 1)}")
                    # save the product to csv file
                    with open('products.csv', 'a', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow([Product_Name, Product_Price, Product_IsAvailable, Product_IsInStock])
                else:
                    # log the product name to a file if the product is not available
                    logging.error(f"Error while parsing product {i['Products'][0]['Name']} is not available")
                    # log the error to a file
                    with open('NotAvailable.txt', 'a') as file:
                        file.write(f"Error while parsing product {i['Products'][0]['Name']} is not available")
                        file.write("\n")
            except KeyError:
                # log an error if the product available key is not found to a file
                logging.error(f"Error while parsing product {i['Products'][0]['Name']} no clue is the product available or not")
                # log the error to a file
                with open('error.txt', 'a') as file:
                    file.write(f"Error while parsing product {i['Products'][0]['Name']} no clue is the product available or not")
                    file.write("\n")

if __name__ == '__main__':
    products = Products()
    products.import_json()
    products.PrintsAndSave()

            