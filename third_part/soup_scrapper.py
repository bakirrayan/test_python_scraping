######## Exercise 3 ########

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup
import csv

# create a webdriver instance
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
# get the page
driver.get('https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas')
# wait for the page to load
sleep(20)

# get the page source
content = driver.page_source
soup = BeautifulSoup(content, features="html.parser")

# find all the products
product = soup.find_all(class_= 'product-tile-title')
breadcrumb = soup.find_all(class_= 'breadcrumbs-linkList')

# add to a csv file with the name of the product
for p in product:
    with open('products_scraped.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([p.text])

for x in breadcrumb:
    out = []
    for li in x.findAll('li'):
       out.append(li.text) 
    print(out)