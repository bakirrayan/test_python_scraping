from numpy import prod

def exercise_one():
    for i in range(1, 101):
        # check if i is divisible by 3 and 5 then print ThreeFive
        if i % 3 == 0 and i % 5 == 0:
            print("ThreeFive")
        # check if i is divisible by 3 only then print Three
        elif i % 3 == 0:
            print("Three")
        # check if i is divisible by 5 only then print Five
        elif i % 5 == 0:
            print("Five")
        # if i is not divisible by 3 or 5 then print i
        else:
            print(i)

def exercise_two(num:int):
    # convert num to list of integers
    i = [int(x) for x in str(num)]
    # get product of all elements in i
    j = prod(i)
    # append product of each element with the next element
    for y in range(len(i)-1):
        i.append(i[y]*i[y+1])
    i.append(j)
    # check if length of i is equal to length of set of i that means there is no duplicate and the number colorful
    return len(i) == len(set(i))

# exercise_three
def calculate(a:list):
    # check if a is a list
    if type(a) != list:
        return False
    else:
        out = []
        for i in a:
            # check if i is a string
            if type(i) == str:
                try:
                    out.append(int(i))
                except:
                    pass
        # return sum of all elements in out
        return sum(out)

# exercise_four
def anagrams(word, words):
    # check if there is an anagram of word in words
    return [i for i in words if sorted(i) == sorted(word)]