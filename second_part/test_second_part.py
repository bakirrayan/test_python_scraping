import pytest

from src import div, raise_something, add, ForceToList, random_gen, get_info, CacheDecorator, ProcessChecker


def test_generator():
    g = random_gen()
    assert isinstance(g, type((x for x in [])))
    a = next(g)
    while a != 15:
        assert 10 <= a <= 20
        a = next(g)
    with pytest.raises(StopIteration):
        next(g)


def test_to_str():
    assert add(5, 30) == '35'
    assert get_info({'info': [1, 2, 3]}) == '[1, 2, 3]'


def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)


def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4

######## Exercise 4 ########
def adds(a, b):
    return a + b

def test_cache_decorator():
    # We create a cache decorator
    cache = CacheDecorator()
    # We call the function with the decorator
    out_1 = cache(adds)(1, 3)
    # We check that the result is correct
    assert cache.cache[1] == 4 

    # we test the KeyError exception 
    with pytest.raises(KeyError) as e:
        # We create a cache decorator
        cache = CacheDecorator()
        # We call the function with the decorator
        out_2 = cache(adds)(4, 5)
        cache.cache[5] == 20 # Not working because the key is 5 insted of 4
    assert e.type == KeyError



