import inspect
from random import randint

######## Exercise 1 ########
def random_gen():
    # each time the generator is called, it should return a new random number
    x = 0
    while x!=15:
        x = randint(10,20)
        yield x

######## Exercise 2 ########    
def decorator_to_str(func):
    # we create a wrapper function that will take all the arguments of the function and return a string of the result of the function
    def wrapper(*a,**kw):
        string = str(func(*a,**kw))
        return string
    return wrapper


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']

######## Exercise 3 ########
def ignore_exception(exception):
    # we create a decorator that will ignore the exception of the function and return None
    def decorator(func):
        # we create a wrapper function that will take all the arguments of the function and return None if the exception is raised
        def wrapper(*a, **kw):
            try:
                return func(*a, **kw)
            except exception:
                return None
        return wrapper
    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


######## Exercise 4 ########
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

######## Exercise 5 ########
class MetaInherList(type):
    # we create __new__ method to add list as a base class
    def __new__(cls, name, bases, attrs):
        bases_with_list = [base for base in bases if base != list] + [list]
        new_class = super().__new__(cls, name, tuple(bases_with_list), attrs)
        
        # we create __init__ method to add the default value of x if it is not given
        def __init__(self, *args, **kwargs):
            if not args:
                if hasattr(Ex, 'x'):
                    default_value = Ex.x
                args = ([default_value],)
            super(new_class, self).__init__(*args, **kwargs)

        # we set the attribute of list to the new class
        for attr in dir(list):
            if not attr.startswith("__"):
                setattr(new_class, attr, getattr(list, attr))
        return new_class

class Ex:
    x = 4

class ForceToList(Ex, metaclass=MetaInherList):
    pass

######## Exercise 6 ########
class ProcessChecker(type):
    def __new__(cls, name, bases, attrs):
        # we check if the class has a process method and if it is callable
        if 'process' in attrs:
            process_method = attrs['process']
            # we check if the process is a method and if it has 3 arguments
            if not inspect.ismethod(process_method):
                raise TypeError(f"'process' attribute in class {name} is not a method.")
            if len(process_method.__code__.co_varnames) != 3:
                raise ValueError(f"process method in class {name} must have 3 arguments")
        else:
            # we raise an error if the process method is not found
            raise AttributeError(f"process method not found in class {name}")
            
        return super().__new__(cls, name, bases, attrs)
    
# Test for exercise 6
# to execute this test, uncomment one of the class to test the different errors
# then in the terminal, run the command: "python" and then "from src import *" the error will be raised

#class TestClass1(metaclass=ProcessChecker):
#    # there is no attribute process in the class
#    pass
#class TestClass2(metaclass=ProcessChecker):
#    # the number of arguments is not 3
#    def process(arg1, arg2):
#        pass
#class TestClass3(metaclass=ProcessChecker):
#    # There is no error
#    def process(arg1, arg2, arg3):
#        pass
#class TestClass4(metaclass=ProcessChecker):
#    # process attribute is not a method
#    process = 1